#création des machines
docker-machine create --driver virtualbox gerant
docker-machine create --driver virtualbox travail

#activation du swarm
docker-machine ssh gerant "docker swarm init --advertise-addr 192.168.99.110"

#Commande qui permet de joindre une machine au cluster Swarm
docker-machine ssh travail "docker swarm join \--token SWMTKN-1-30jbvqeqptqpokilxty9ro6wyjnawx0zju09qdek5w07dsbsjt-8c6nh8ar2hczb010bo7nlmrp5 192.168.99.110:2377"

#Configuration du shell du leader swarm
docker-machine env gerant

eval $(docker-machine env gerant)
docker-machine active


#création de l'image Tomcat

docker build -t geoserver .

#lancement du container grâce au Dockerfile
docker run -d -p 8080:80 --name geoserver geoserver

#création du service
docker service create -p 8080:8080 geoserver geoserver

#permet de voir les services en routes
docker service ls

#Permet de dupliquer l'image sur le swarm
docker service scale geoserver=2
